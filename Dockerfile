FROM ubuntu:18.04 as main

ARG DEBIAN_FRONTEND=noninteractive
RUN apt update -y && apt install -y \
	wget \
	tar \
	make \
	git \
	patch \
	xz-utils \
	gcc \
	bc \
	xxd \
	build-essential \
	bison \
	flex \
	python3 \
	python3-distutils \
	python3-dev \
	swig \
	python \
	python-dev \
	kmod \
	curl \
	u-boot-tools \
	p7zip-full

RUN git config --global user.email "you@example.com"
RUN git config --global user.name "Your Name"
RUN git config --global color.ui false

# Get mkdtimg
RUN wget https://android.googlesource.com/platform/system/libufdt/+archive/refs/heads/master/utils.tar.gz
RUN tar xvf utils.tar.gz
RUN cp src/mkdtboimg.py /usr/bin/mkdtimg
RUN chmod a+x /usr/bin/mkdtimg
RUN rm -rf src utils.tar.gz tests README.md

VOLUME /build
WORKDIR /build
COPY . /build

# ARM64
FROM main as build_arm64
ONBUILD RUN apt install -y gcc-aarch64-linux-gnu

# AMD64
FROM main as build_amd64

FROM build_${TARGETARCH}
CMD /build/build.sh
