#!/bin/bash
set -xe

# Build variables
export KBUILD_BUILD_USER=${KBUILD_BUILD_USER:-"user"}
export KBUILD_BUILD_HOST=${KBUILD_BUILD_HOST:-"custombuild"}
export ARCH=${ARCH:-"arm64"}
export CPUS=${CPUS:-$(($(getconf _NPROCESSORS_ONLN) - 1))}
export CWD="$(realpath "$(dirname "${BASH_SOURCE[0]}")")"
export KERNEL_DIR="${CWD}/kernel"
export URL=${URL:-"https://gitlab.com/l4t-community/kernel/l4t-kernel-4.9"}

export KERNEL_BRANCH=${KERNEL_BRANCH:-"fedora-dev"}
export DT_VER=${DT_VER:-"l4t/l4t-r32.5"}
export NX_VER=${NX_VER:-"linux-5.1.2"}
export NV_VER=${NX_VER:-"linux-5.1.2"}
export NVGPU_VER=${NVGPU_VER:-"linux-3.4.0-r32.5"}

# Create compressed modules and update archive with correct permissions and ownership 
create_update_modules() {
	find "$1" -type d -exec chmod 755 {} \;
	find "$1" -type f -exec chmod 644 {} \;
	find "$1" -name "*.sh" -type f -exec chmod 755 {} \;
	fakeroot chown -R root:root "$1"
	tar -C "$1" -czvpf "$2" .
}

Prepare() {
	# Clone L4T kernel from l4t-community
	if [[ -z `ls -A ${KERNEL_DIR}/kernel-4.9` ]]; then
		git clone -b "${KERNEL_BRANCH}" "${URL}" "${KERNEL_DIR}/kernel-4.9"
	fi

	if [[ -z $(ls -A ${KERNEL_DIR}/nvidia) ]]; then
		git clone -b ${NV_VER} https://gitlab.com/l4t-community/kernel/switch-l4t-kernel-nvidia "${KERNEL_DIR}/nvidia"
		git clone -b ${NVGPU_VER} https://gitlab.com/switchroot/kernel/l4t-kernel-nvgpu "${KERNEL_DIR}/nvgpu"
		git clone -b ${NX_VER} https://github.com/CTCaer/switch-l4t-platform-t210-nx "${KERNEL_DIR}/hardware/nvidia/platform/t210/nx"
		git clone -b ${DT_VER} https://gitlab.com/switchroot/kernel/l4t-soc-t210 "${KERNEL_DIR}/hardware/nvidia/soc/t210"
		git clone -b ${DT_VER} https://gitlab.com/switchroot/kernel/l4t-soc-tegra "${KERNEL_DIR}/hardware/nvidia/soc/tegra/"
		git clone -b ${DT_VER} https://gitlab.com/switchroot/kernel/l4t-platform-tegra-common "${KERNEL_DIR}/hardware/nvidia/platform/tegra/common/"
		git clone -b ${DT_VER} https://gitlab.com/switchroot/kernel/l4t-platform-t210-common "${KERNEL_DIR}/hardware/nvidia/platform/t210/common/"
	fi

	# Setup linaro aarch64 GCC7 for cross compilation if needed
	if [[ `uname -m` != aarch64 ]]; then
		if [[ ! -d "${KERNEL_DIR}/gcc-linaro-7.5.0-2019.12-x86_64_aarch64-linux-gnu" ]]; then
			echo -e "\nSetting up aarch64 cross compiler"
			wget -q -nc --show-progress https://releases.linaro.org/components/toolchain/binaries/latest-7/aarch64-linux-gnu/gcc-linaro-7.5.0-2019.12-x86_64_aarch64-linux-gnu.tar.xz
			tar xf gcc-linaro-7.5.0-2019.12-x86_64_aarch64-linux-gnu.tar.xz -C "${KERNEL_DIR}"
			rm gcc-linaro-7.5.0-2019.12-x86_64_aarch64-linux-gnu.tar.xz
		fi

		# Set cross compiler in PATH and CROSS_COMPILE string
		export PATH="$(realpath ${KERNEL_DIR}/gcc-linaro-7.5.0-2019.12-x86_64_aarch64-linux-gnu)/bin/:$PATH"
		export CROSS_COMPILE=${CROSS_COMPILE:-"aarch64-linux-gnu-"}
	fi

	# Retrieve mkdtimg
	if [[ ! -e "${KERNEL_DIR}/mkdtimg" ]]; then
		wget https://android.googlesource.com/platform/system/libufdt/+archive/refs/heads/master/utils.tar.gz
		tar xvf utils.tar.gz
		cp src/mkdtboimg.py "${KERNEL_DIR}/mkdtimg"
		chmod a+x "${KERNEL_DIR}/mkdtimg"
		rm -rf utils.tar.gz tests src README.md
	fi
	export PATH="$(realpath ${KERNEL_DIR}):$PATH"
}

Build() {
	echo "Preparing Source and Creating Defconfig"

	cd "${KERNEL_DIR}/kernel-4.9"
	make tegra_linux_defconfig
	make -j${CPUS} prepare tegra-dtstree="../hardware/nvidia"
	make -j${CPUS} zImage tegra-dtstree="../hardware/nvidia"
	make -j${CPUS} dtbs tegra-dtstree="../hardware/nvidia"
	make -j${CPUS} modules tegra-dtstree="../hardware/nvidia"

	mkimage -A arm64 -O linux -T kernel -C gzip -a 0x80200000 -e 0x80200000 -n "AZKRN-${KERNEL_BRANCH}" -d arch/arm64/boot/zImage "${KERNEL_DIR}/uImage"

	mkdtimg create "${KERNEL_DIR}/nx-plat.dtimg" --page_size=1000 \
        arch/arm64/boot/dts/tegra210-odin.dtb	 --id=0x4F44494E \
		arch/arm64/boot/dts/tegra210b01-odin.dtb --id=0x4F44494E --rev=0xb01 \
		arch/arm64/boot/dts/tegra210b01-vali.dtb --id=0x56414C49 \
		arch/arm64/boot/dts/tegra210b01-fric.dtb --id=0x46524947

	make modules_install INSTALL_MOD_PATH="${KERNEL_DIR}/modules/" tegra-dtstree="../hardware/nvidia"
	make headers_install INSTALL_HDR_PATH="${KERNEL_DIR}/update/usr/" tegra-dtstree="../hardware/nvidia"
}

PostConfig() {
	# Refresh permissions for kernel headers and Create compressed modules and headers
	find "${KERNEL_DIR}/update/usr/include" -name *.install* -exec rm {} \;
	find "${KERNEL_DIR}/update/usr/include" -exec chmod 777 {} \;

	ln -sfn /usr/src/kernel-4.9 "${KERNEL_DIR}/modules/lib/modules/4.9.140-l4t+/build"

	create_update_modules "${KERNEL_DIR}/modules/lib/" "${KERNEL_DIR}/modules.tar.gz"
	create_update_modules "${KERNEL_DIR}/update/" "${KERNEL_DIR}/update.tar.gz"

	7z a "${KERNEL_DIR}/kernel.7z" "${KERNEL_DIR}/modules.tar.gz" "${KERNEL_DIR}/update.tar.gz" "${KERNEL_DIR}/uImage" "${KERNEL_DIR}/nx-plat.dtimg"

	rm -rf "${KERNEL_DIR}/modules" "${KERNEL_DIR}/update" "${KERNEL_DIR}/modules.tar.gz" "${KERNEL_DIR}/update.tar.gz" "${KERNEL_DIR}/uImage" "${KERNEL_DIR}/nx-plat.dtimg"
	echo "Done"
}

Prepare
Build
PostConfig
